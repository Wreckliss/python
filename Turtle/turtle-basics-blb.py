import turtle
# main function
def main():
	screen = turtle.Screen()
	t = turtle.Turtle()
	#turtle.tracer(0, 0)
	t.pencolor("#ff0000")#red
	t.fd(50)
	t.lt(420)
	t.pencolor("#00ff00")#green
	t.width(21)
	t.bk(69)
	t.rt(25)
	t.pencolor("#0000ff")#blue
	t.width(2)
	t.goto(-10,0)
	t.penup()

	# fill poly start
	t.goto(0,0)
	t.pendown()
	t.pencolor("#00ffff")#cyan
	t.begin_fill()
	t.fd(56)
	t.rt(20)
	t.fd(10)
	t.rt(150)
	t.fd(50)
	t.rt(230)
	t.fd(100)
	t.end_fill()
	
	t.penup()
	t.goto(-100,100)
	t.pendown()
	t.pencolor("#ff00ff")#magenta
	t.fillcolor("#ff7f00")#orange
	t.begin_fill()
	t.fd(8)
	t.rt(520)
	t.fd(120)
	t.rt(145)
	t.fd(151)
	t.rt(932)
	t.fd(1154)
	t.end_fill()
	t.circle(10)
	t.pencolor("#ffff00")
	t.circle(20)
	t.pendown()
	t.goto(-200,-200)
	t.fillcolor("#00cc7f")
	t.begin_fill()
	t.circle(251215)
	t.end_fill()
	
	screen.exitonclick()	
	

if __name__ == "__main__":
		main()
